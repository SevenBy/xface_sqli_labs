# xface_sqli_labs

### 本版本SQLilabs fork自github 《SQLilabs》作者原创版本，因为php版本问题作以修改，为了兼容php7 系列版本。

### 配置环境

系统Debian 9.x 64 bit , 以下命令中可能有多余的命令

### 配置Apache：
```
sudo apt install apache2

sudo apt install apache2-dev  

打开浏览器，输入localhost可验证是否安装成功
```

### 配置mysql：
```
sudo apt install mysql-server mysql-client

sudo apt install php-mysql

sudo apt install php7.2-mysql
```

---



我下的这个默认版本是mysql 5.7，安装的时候root默认密码是空，同时，如果是你直接mysql -uroot -p然后按空格的话会提示你access denied的，原因是进行了权限检查

解决办法是：

sudo mysql //这个命令可以直接进去，另一个进去的方法是使用/etc/mysql/debian.cnf这个文件自动生成的账号密码

```
UPDATE mysql.user SET authentication_string=PASSWORD('root'), PLUGIN='mysql_native_password' WHERE USER='root';
```

//设置root密码为root，PLUGIN='mysql_native_password'意思是关闭access denied的ya验证

### 配置PHP：
```
sudo apt install php 
//sudo apt install php 现在是下载7.2版本，所以下面都是7.2

sudo apt install php-mysql

sudo apt install libapache2-mod-php

sudo apt-get install libapache2-mod-php7.2 php7.2-gd php7.2-mbstring php7.2-xml php7.2-mysql php7.2-dev
```


sqli-libs的源码是基于PHP5的和PHP7的兼容有问题，详细请看https://github.com/Audi-1/sqli-labs/issues/11。在Issue里已经有人对这个兼容问题已经做了解决，“MySQL functions were removed in PHP 7, but we can use MySQLi functions instead.”，并且贴出了兼容后的代码，所以直接使用：

### 更改sqli-libs用的默认数据库用户名和密码

```
sudo vim /sqli-labs/sql-connections/db-creds.inc

使用浏览器打开localhost\sqli-labs\，点击安装数据库
```

Enjoy it
--------------------- 